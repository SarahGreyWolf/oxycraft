use crate::item::Tool;

enum TickableTiles {
    Furnace,

}

struct Block;

struct Mineable(u8, Tool);

struct LightEmitter(u8);

/// Tickable with how often it should be ticked
struct Tickable(u32);

// TODO: Flesh Out, needs to handle it's slots, or slots as own component?
struct Inventory;
