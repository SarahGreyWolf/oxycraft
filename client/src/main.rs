use bevy::{app::AppExit, prelude::*};

mod main_menu;

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
enum GameState {
    MainMenu,
    InGame,
    Paused,
    Quit
}

fn main() {
    App::build()
        .insert_resource(Msaa { samples: 4 })
        .add_plugins(DefaultPlugins)
        .add_state(GameState::MainMenu)
        .add_plugin(main_menu::MenuPlugin)
        .add_system_set(
            SystemSet::on_enter(GameState::InGame)
                .with_system(game_setup.system())
        )
        .add_system_set(
            SystemSet::on_enter(GameState::Quit)
                .with_system(game_quit.system())
        )
        .run();
}

/// set up a simple 3D scene
fn game_setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    // plane
    commands.spawn_bundle(PbrBundle {
        mesh: meshes.add(Mesh::from(shape::Plane { size: 5.0 })),
        material: materials.add(Color::rgb(0.3, 0.5, 0.3).into()),
        ..Default::default()
    });
    // cube
    commands.spawn_bundle(PbrBundle {
        mesh: meshes.add(Mesh::from(shape::Cube { size: 1.0 })),
        material: materials.add(Color::rgb(0.8, 0.7, 0.6).into()),
        transform: Transform::from_xyz(0.0, 0.5, 0.0),
        ..Default::default()
    });
    // light
    commands.spawn_bundle(LightBundle {
        transform: Transform::from_xyz(4.0, 8.0, 4.0),
        ..Default::default()
    });
    // camera
    commands.spawn_bundle(PerspectiveCameraBundle {
        transform: Transform::from_xyz(-2.0, 2.5, 5.0).looking_at(Vec3::ZERO, Vec3::Y),
        ..Default::default()
    });
}

fn game_quit(
    mut commands: Commands,
    entities: Query<Entity>,
    mut exit_events: EventWriter<AppExit>,
) {
    for entity in entities.iter() {
        commands.entity(entity).despawn_recursive();
    }
    exit_events.send(AppExit);
}