use bevy::{app::AppExit, prelude::*};
use crate::GameState;

pub struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut bevy::prelude::AppBuilder) {
        app
            .init_resource::<ButtonMaterials>()
            .insert_resource(ClearColor::default())
            .add_system_set(
                SystemSet::on_enter(GameState::MainMenu)
                    .with_system(setup.system())
            )
            .add_system_set(
                SystemSet::on_update(GameState::MainMenu)
                    .with_system(menu_buttons.system())
            )
            .add_system_set(
                SystemSet::on_exit(GameState::MainMenu)
                    .with_system(exit.system())
            );
    }
}

enum ButtonType {
    Play,
    Multiplayer,
    Options,
    Quit
}

struct ButtonMaterials {
    normal:  Handle<ColorMaterial>,
    hovered: Handle<ColorMaterial>,
    pressed: Handle<ColorMaterial>,
}

impl FromWorld for ButtonMaterials {
    fn from_world(world: &mut World) -> Self {
        let mut materials = world.get_resource_mut::<Assets<ColorMaterial>>().unwrap();
        Self {
            normal: materials.add(Color::rgb(0.15, 0.15, 0.15).into()),
            hovered: materials.add(Color::rgb(0.35, 0.35, 0.35).into()),
            pressed: materials.add(Color::rgb(0.55, 0.55, 0.55).into()),
        }
    }
}

fn menu_buttons(
    mut app_state: ResMut<State<GameState>>,
    button_mats: Res<ButtonMaterials>,
    mut interaction_query: Query<
        (&Interaction, &mut Handle<ColorMaterial>, &Children, &ButtonType),
        (Changed<Interaction>, With<Button>),
    >,
    mut text_query: Query<&mut Text>,
) {
    for (interaction, mut mat, children, button) in interaction_query.iter_mut() {
        let mut _text = text_query.get_mut(children[0]).unwrap();
        match *interaction {
            Interaction::Clicked => {
                *mat = button_mats.pressed.clone();
                match button {
                    ButtonType::Play => {
                        // TODO: Possibly a loading state
                        app_state.set(GameState::InGame).unwrap();
                    }
                    ButtonType::Quit => {
                        app_state.set(GameState::Quit).unwrap();
                    }
                    _ => {  }
                }
            }
            Interaction::Hovered => {
                *mat = button_mats.hovered.clone();
            }
            Interaction::None => {
                *mat = button_mats.normal.clone();
            }
        }
    }
}

fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    button_mats: Res<ButtonMaterials>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    clear_color: Res<ClearColor>
) {
    let button_size: Size<Val> = Size::new(Val::Px(200.0), Val::Px(65.0));
    let button_style = Style {
        align_items: AlignItems::Center,
        justify_content: JustifyContent::Center,
        margin: Rect {
            left: Val::Auto,
            right: Val::Auto,
            top: Val::Auto,
            bottom: Val::Px(40.0),
        },
        size: button_size.clone(), 
        ..Default::default()
    };
    commands.spawn_bundle(UiCameraBundle::default());
    commands
        .spawn_bundle(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::ColumnReverse,
                align_items: AlignItems::Center,
                margin: Rect::all(Val::Auto),
                position: Rect {
                    left: Val::Auto,
                    right: Val::Auto,
                    top: Val::Percent(10.0),
                    bottom: Val::Auto,
                },
                ..Default::default()
            },
            material: materials.add(ColorMaterial{
                color: clear_color.0,
                ..Default::default()
            }),
            ..Default::default()
        })
        .with_children(|parent| {
            
            parent
                .spawn_bundle(ButtonBundle {
                    style: button_style.clone(),
                    material: button_mats.normal.clone(),
                    ..Default::default()
                })
                .insert(ButtonType::Play)
                .with_children(|parent| {
                    parent.spawn_bundle(TextBundle {
                        text: Text::with_section(
                            "Play",
                            TextStyle {
                                font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                                font_size: 40.0,
                                color: Color::rgb(0.9, 0.9, 0.9),
                            },
                            Default::default()
                        ),
                        ..Default::default()
                    });
                });

            parent
                .spawn_bundle(ButtonBundle {
                    style: button_style.clone(),
                    material: button_mats.normal.clone(),
                    ..Default::default()
                })
                .insert(ButtonType::Multiplayer)
                .with_children(|parent| {
                    parent.spawn_bundle(TextBundle {
                        text: Text::with_section(
                            "Multiplayer",
                            TextStyle {
                                font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                                font_size: 40.0,
                                color: Color::rgb(0.9, 0.9, 0.9),
                            },
                            Default::default()
                        ),
                        ..Default::default()
                    });
                });

            parent
                .spawn_bundle(ButtonBundle {
                    style: button_style.clone(),
                    material: button_mats.normal.clone(),
                    ..Default::default()
                })
                .insert(ButtonType::Quit)
                .with_children(|parent| {
                    parent.spawn_bundle(TextBundle {
                        text: Text::with_section(
                            "Quit Game",
                            TextStyle {
                                font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                                font_size: 40.0,
                                color: Color::rgb(0.9, 0.9, 0.9),
                            },
                            Default::default()
                        ),
                        ..Default::default()
                    });
                });
        });
}

fn exit(mut commands: Commands, entities: Query<Entity>) {
    for entity in entities.iter() {
        commands.entity(entity).despawn_recursive();
    }
}
