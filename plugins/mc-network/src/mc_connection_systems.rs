use bevy::prelude::*;
use bevy::tasks::TaskPool;

use crate::craftio_resource::CraftioResource;
use crate::McNetworkEvent;

fn send_mcnetwork_message(
    mut commands: Commands,
    mut evs: EventReader<McNetworkEvent>,
    mut task_pool: ResMut<TaskPool>,
    mut mc_connection: ResMut<CraftioResource>,
) {
    for ev in evs.iter() {
        match ev {
            McNetworkEvent::RequestConnection(addr) => {
                // let (client_tx, network_rx) = crossbeam_channel::unbounded();
                // let (network_tx, client_rx) = crossbeam_channel::unbounded();
                let addr = addr.clone();

                tokio::task::spawn(async move { unimplemented!("Uninmplemented") });
            }
            McNetworkEvent::RequestDisconnection => {}
            McNetworkEvent::ConnectionResponse(_) => {}
        }
    }
}
