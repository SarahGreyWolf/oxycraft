use bevy::prelude::{AppBuilder, Plugin};

mod craftio_resource;
mod mc_connection_systems;

pub enum McNetworkEvent {
    RequestConnection(String),
    ConnectionResponse(bool),
    RequestDisconnection,
}
pub struct McNetworkPlugin;

impl Plugin for McNetworkPlugin {
    fn build(&self, app: &mut AppBuilder) {}
}
