use craftio_rs::CraftTokioConnection;
use crossbeam_channel::{Receiver, Sender};
use mcproto_rs::v1_16_3::Packet753;

use crate::McNetworkEvent;

pub struct CraftioResource {
    client_rx: Sender<McNetworkEvent>,
    client_tx: Receiver<McNetworkEvent>,
    network_rx: Sender<McNetworkEvent>,
    network_tx: Receiver<McNetworkEvent>,
}

impl CraftioResource {
    pub fn new() -> Self {
        let (c_rx, c_tx) = crossbeam_channel::unbounded();
        let (n_rx, n_tx) = crossbeam_channel::unbounded();

        CraftioResource {
            client_rx: c_rx,
            client_tx: c_tx,
            network_rx: n_rx,
            network_tx: n_tx,
        }
    }

    pub fn create_connection(&self, addr: String) {
        let addr = addr.clone();
        let client_tx = self.client_tx.clone();
        let network_rx = self.network_rx.clone();

        tokio::task::spawn(async move {
            let conn = CraftTokioConnection::connect_server_tokio(addr.clone())
                .await
                .expect("Failed to establish connection");
            // conn.write_packet_async(Packet753::Handshake());
        });
    }
}
